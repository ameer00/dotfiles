#kubectl_aliases
function kubectl() { echo "+ kubectl $@">&2; command kubectl $@; }
[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases

# Example aliases
alias ohmyzsh="nano ~/.oh-my-zsh"
alias c="clear"
alias update="source ~/.zshrc"
alias ls="ls --color=auto"
alias b="sudo byobu"
alias bn="sudo byobu new -s"
alias bl="sudo byobu list-sessions"
alias ba="sudo byobu attach-session -t"
alias bkill="sudo byobu kill-window -t"
alias bkillserver="sudo byobu kill-server"
alias kctx="kubectl ctx"
alias kens="kubectl ns"
alias kistio="kubens istio-system"
alias em="emacsclient"
alias emacsd="emacs --daemon"

## git aliases
function gc { git commit -m "$@"; }
alias gcm="git checkout master";
alias gs="git status";
alias gpull="git pull";
alias gf="git fetch";
alias gfa="git fetch --all";
alias gf="git fetch origin";
alias gpush="git push";
alias gd="git diff";
alias ga="git add .";
alias gb="git branch";
alias gbr="git branch remote"
alias gfr="git remote update"
alias gbn="git checkout -B "
alias grf="git reflog";
alias grh="git reset HEAD~" # last commit
alias gac="git add . && git commit -a -m "
alias gsu="git gpush --set-upstream origin "
alias glog="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --branches"

## other aliases
alias zshrc='code ~/.zshrc'
alias topten="history | commands | sort -rn | head"
alias myip="curl http://ipecho.net/plain; echo"
alias dirs='dirs -v | head -10'
alias usage='du -h -d1'
alias update="source ~/.zshrc"
alias sshdir="cd ~/.ssh"
alias sshls="ll ~/.ssh"
alias runp="lsof -i "
alias md="mkdir "
alias ..='cd ..'
alias ...='cd ../..'
alias ll="ls --color=auto -1a";
alias la="ls --color=auto -al";
alias ls="ls --color=auto";
alias l="ls --color=auto -a";
alias ..l="cd ../ && ll";
alias pg="echo 'Pinging Google' && ping www.google.com";
alias code='/home/coder/.vscode-server/bin/8795a9889db74563ddd43eb0a897a2384129a619/bin/code'
alias gsetproj='gcloud config set project'