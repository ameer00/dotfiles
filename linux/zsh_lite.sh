cd $HOME

cat <<EOF > $HOME/.customize_environment
#!/bin/bash
sudo apt update
sudo apt -y install tree zsh
EOF

sudo apt update && sudo apt install -y tree zsh && echo -e "export PATH=$PATH:~/.local/bin" | tee -a ~/.bashrc && source ~/.bashrc

git config --global user.email "ameer00"
git config --global user.name "ameer00"
 
echo -e "command -v zsh >/dev/null 2>&1 && { echo >&2 "Entering ZSH...."; zsh;}" | tee -a ~/.profile
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sudo chsh -s $HOME/.local/bin/zsh

cat <<'EOF' > $HOME/zsh_install.sh
export ZSH_CUSTOM=$HOME/.oh-my-zsh/custom
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
sed -i s/'plugins=(git)'/'plugins=(git zsh-autosuggestions zsh-syntax-highlighting kubectl)'/g ~/.zshrc

cat <<'EOT' >> ~/.zshrc
### Fix slowness of pastes with zsh-syntax-highlighting.zsh
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=5'
pasteinit() {
OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
### Fix slowness of pastes
### Fix adding backslashes while copying and pasting
zstyle ':urlglobber' url-other-schema
source $ZSH/oh-my-zsh.sh
EOT

(
set -x; cd "$(mktemp -d)" &&
curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
tar zxvf krew.tar.gz &&
KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
$KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
$KREW update
)
export PATH="${PATH}:${HOME}/.krew/bin"
echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.zshrc

echo -e "ctx\nns\npod-logs\npod-shell\nresource-capacity\nget-all\nimages\nsniff\ntree\ntail\nneat" > $HOME/.krew_plugins.txt
kubectl krew install < $HOME/.krew_plugins.txt

wget -O $HOME/.zsh_aliases.sh https://gitlab.com/ameer00/dotfiles/-/raw/master/linux/.zsh_aliases.sh
echo -e "source ~/.zsh_aliases.sh" | tee -a ~/.zshrc

mkdir -p $HOME/utils; cd $HOME/utils
mkdir -p $HOME/.local/bin
echo -e "export PATH=$PATH:/$HOME/.local/bin:/$HOME/.linuxbrew/bin" | tee -a ~/.zshrc
source ~/.zshrc

wget https://github.com/neovim/neovim/releases/download/v0.4.3/nvim-linux64.tar.gz
tar -xzvf nvim-linux64.tar.gz
rm -rf nvim-linux64.tar.gz
ln -s $HOME/utils/nvim-linux64/bin/nvim $HOME/.local/bin/nvim
python3 -m pip install --user --upgrade pynvim
mkdir -p $HOME/.vim/files/info

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

mkdir -p $HOME/.vim/files/info
mkdir -p $HOME/.config/nvim
wget -O $HOME/.config/nvim/init.vim https://gitlab.com/ameer00/dotfiles/-/raw/master/linux/init_light.vim
echo -e "\nset viminfo='100,n$HOME/.vim/files/info/viminfo" | tee -a $HOME/.config/nvim/init.vim

nvim -es -u $HOME/.config/nvim/init.vim -i NONE -c "PlugInstall" -c "qa"
nvim -es -u $HOME/.config/nvim/init.vim -i NONE -c "PlugInstall" -c "qa"

mkdir -p $HOME/.local/share/nvim/site/pack/coc/start
cd ~/.local/share/nvim/site/pack/coc/start
curl --fail -L https://github.com/neoclide/coc.nvim/archive/release.tar.gz | tar xzfv -
mkdir -p $HOME/.config/coc/extensions
cd $HOME/.config/coc/extensions
echo '{"dependencies":{}}'> package.json
npm install coc-snippets coc-pairs coc-yaml coc-prettier coc-json --global-style --ignore-scripts --no-bin-links --no-package-lock --only=prod
nvim -es -u $HOME/.config/nvim/init.vim -i NONE -c 'CocInstall coc-snippets coc-pairs coc-yaml coc-prettier coc-json' -c "qa"

echo -e "alias vi='nvim'" | tee -a ~/.zsh_aliases.sh

cd ~ && git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all
echo -e "[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh" | tee -a ~/.zshrc
EOF
chmod +x zsh_install.sh

/usr/bin/zsh <<'EOF'
cd $HOME
./zsh_install.sh
clear
rm -rf zsh*
EOF

zsh

