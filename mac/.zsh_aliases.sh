# Example aliases
alias ohmyzsh="nano ~/.oh-my-zsh"
alias c="clear"
alias update="source ~/.zshrc"
alias ls="ls -l"
alias b="byobu"
alias bn="byobu new -s"
alias bkill="byobu kill-server"
alias kctx="kubectx"
alias kens="kubens"
alias kistio="kubens istio-system"
alias work="ssh ameerabbas@work.platformx.dev"
alias code="/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code"
alias vim="mvim"

## git aliases
function gc { git commit -m "$@"; }
alias gcm="git checkout master";
alias gs="git status";
alias gpull="git pull";
alias gf="git fetch";
alias gfa="git fetch --all";
alias gf="git fetch origin";
alias gpush="git push";
alias gd="git diff";
alias ga="git add .";
alias gb="git branch";
alias gbr="git branch remote"
alias gfr="git remote update"
alias gbn="git checkout -B "
alias grf="git reflog";
alias grh="git reset HEAD~" # last commit
alias gac="git add . && git commit -a -m "
alias gsu="git gpush --set-upstream origin "
alias glog="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --branches"

## other aliases
alias zshrc='vim ~/.zshrc'
alias vimrc='vim ~/.vimrc'
alias topten="history | commands | sort -rn | head"
alias myip="curl http://ipecho.net/plain; echo"
alias dirs='dirs -v | head -10'
alias usage='du -h -d1'
alias update="source ~/.zshrc"
alias sshdir="cd ~/.ssh"
alias sshls="ll ~/.ssh"
alias runp="lsof -i "
alias md="mkdir "
alias ..='cd ..'
alias ...='cd ../..'
alias ll="colorls -1a";
alias la="colorls -al";
alias ls="colorls -a";
alias l="colorls -a";
alias ..l="cd ../ && ll";
alias pg="echo 'Pinging Google' && ping www.google.com";

#kubectl_aliases
function kubectl() { echo "+ kubectl $@">&2; command kubectl $@; }
[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases
