# antigen and oh my zsh
source /Users/ameerabbas/.antigen/antigen.zsh
antigen use oh-my-zsh

# Update ZSH every so often
export UPDATE_ZSH_DAYS=13

# autosrc allows to run a script when entering a dir, like setting kubeconfig or python venv
# https://github.com/seanyeh/autosrc
source $HOME/.autosrc.zsh

# Go
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin


antigen bundles <<EOBUNDLES
    git
    osx
    command-not-found
    common-aliases
    python
    pip
    golang
    gcloud
    capistrano
    cp
    emoji
    helm
    history
    sudo
    fzf
    wfxr/forgit
    zsh-users/zsh-autosuggestions
    zsh-users/zsh-syntax-highlighting
    Vifon/deer
EOBUNDLES
autoload -U deer
zle -N deer
bindkey '^k' deer

# Refer to https://github.com/zsh-users/antigen/issues/675 for theme reload problems
THEME=romkatv/powerlevel10k 
antigen list | grep $THEME &> /dev/null; if [ $? -ne 0 ]; then antigen theme $THEME; fi
antigen apply

# This speeds up pasting w/ autosuggest
# https://github.com/zsh-users/zsh-autosuggestions/issues/238
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}
pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
# END This speeds up pasting w/ autosuggest

# my aliases
source ~/.zsh_aliases.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Scroll through substring search history
# Install on mac brew install zsh-history-substring-search and then add below
source /usr/local/share/zsh-history-substring-search/zsh-history-substring-search.zsh

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/ameerabbas/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/ameerabbas/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/ameerabbas/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/ameerabbas/Downloads/google-cloud-sdk/completion.zsh.inc'; fi

# autojump
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
